package io.pillopl.event.storming;


import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
public class CreditCard {

    @Id
    private UUID uuid;
    private BigDecimal initialLimit;
    private BigDecimal usedLimit = BigDecimal.ZERO;
    private int withdrawals;

    public CreditCard(UUID uuid) {
        this.uuid = uuid;
    }

    CreditCard() {

    }

    void assignLimit(BigDecimal amount) {
        if(limitAlreadyAssigned()) {
            throw new IllegalStateException();
        }
        this.initialLimit = amount;
    }



    public void withdraw(BigDecimal amount) {
        if(notEnoughMoneyToWithdraw(amount)) {
            throw new IllegalStateException();
        }

        if(tooManyWithdrawalsInCycle()) {
            throw new IllegalStateException();
        }

        this.usedLimit = usedLimit.add(amount);
        this.withdrawals++;
    }


    void repay(BigDecimal amount) {
        this.usedLimit = usedLimit.subtract(amount);
    }

    void cycleClosed() {
        withdrawals = 0;
    }

    private boolean tooManyWithdrawalsInCycle() {
        return withdrawals >= 45;
    }

    private boolean notEnoughMoneyToWithdraw(BigDecimal amount) {
        return availableLimit().compareTo(amount) < 0;
    }

    private boolean limitAlreadyAssigned() {
        return initialLimit != null;
    }


    public BigDecimal availableLimit() {
        return initialLimit.subtract(usedLimit);
    }
}
