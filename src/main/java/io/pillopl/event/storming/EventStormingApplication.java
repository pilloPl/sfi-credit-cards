package io.pillopl.event.storming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventStormingApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventStormingApplication.class, args);
	}
}
