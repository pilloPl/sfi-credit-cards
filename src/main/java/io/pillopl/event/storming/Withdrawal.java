package io.pillopl.event.storming;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
public class Withdrawal {

    @Id @GeneratedValue private Long id;
    private UUID creditCardUUID;
    private BigDecimal amount;

    public Withdrawal(UUID creditCardUUID, BigDecimal amount) {
        this.creditCardUUID = creditCardUUID;
        this.amount = amount;
    }

    Withdrawal() {
    }

    public UUID getCreditCardUUID() {
        return creditCardUUID;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
