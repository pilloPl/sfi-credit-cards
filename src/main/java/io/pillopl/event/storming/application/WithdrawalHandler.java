package io.pillopl.event.storming.application;

import io.pillopl.event.storming.CreditCard;
import io.pillopl.event.storming.Withdrawal;
import io.pillopl.event.storming.persistance.CreditCardRepository;
import io.pillopl.event.storming.persistance.WithdrawalsRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.UUID;

@Service
public class WithdrawalHandler {

    private final CreditCardRepository creditCardRepository;
    private final WithdrawalsRepository withdrawalsRepository;


    public WithdrawalHandler(CreditCardRepository creditCardRepository, WithdrawalsRepository withdrawalsRepository) {
        this.creditCardRepository = creditCardRepository;
        this.withdrawalsRepository = withdrawalsRepository;
    }

    @Transactional
    public void withdraw(BigDecimal amount, UUID cardNo) {
        CreditCard card = creditCardRepository.getOne(cardNo);
        card.withdraw(amount);
        withdrawalsRepository.save(new Withdrawal(cardNo, amount));
    }
}
