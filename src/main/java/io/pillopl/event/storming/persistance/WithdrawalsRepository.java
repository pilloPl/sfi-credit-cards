package io.pillopl.event.storming.persistance;

import io.pillopl.event.storming.Withdrawal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface WithdrawalsRepository extends JpaRepository<Withdrawal, UUID> {

    List<Withdrawal> findByCreditCardUUID(UUID uuid);

}
