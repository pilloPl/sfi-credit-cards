package io.pillopl.event.storming.ui;

import io.pillopl.event.storming.application.WithdrawalHandler;
import io.pillopl.event.storming.persistance.WithdrawalsRepository;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class WitrhdrawalResource {

    private final WithdrawalsRepository withdrawalsRepository;
    private final WithdrawalHandler withdrawalHandler;
    public WitrhdrawalResource(WithdrawalsRepository withdrawalsRepository, WithdrawalHandler withdrawalHandler) {
        this.withdrawalsRepository = withdrawalsRepository;
        this.withdrawalHandler = withdrawalHandler;
    }

    @PostMapping("/withdrawals")
    public void withdraw(@RequestBody WithdrawalRepresentation wr) {
        withdrawalHandler.withdraw(wr.amount, wr.cardNo);

    }

    @GetMapping("/withdrawals/{cardNo}")
    public List<WithdrawalRepresentation> list(@PathVariable UUID cardNo) {
        return withdrawalsRepository
                .findByCreditCardUUID(cardNo)
                .stream()
                .map(withdrawal -> new WithdrawalRepresentation(withdrawal.getAmount(), withdrawal.getCreditCardUUID()))
                .collect(Collectors.toList());
    }
}

class WithdrawalRepresentation {
    final BigDecimal amount;
    final UUID cardNo;

    WithdrawalRepresentation(BigDecimal amount, UUID cardNo) {
        this.amount = amount;
        this.cardNo = cardNo;
    }
}