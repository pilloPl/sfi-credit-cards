package io.pillopl.event.storming.ui

import io.pillopl.event.storming.CreditCard
import io.pillopl.event.storming.persistance.CreditCardRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class WithdrawalResourceTest extends Specification {

    UUID cardNo = UUID.randomUUID()
    @Autowired WitrhdrawalResource withdrawalResource
    @Autowired CreditCardRepository creditCardRepository

    def setup() {
        CreditCard card = new CreditCard(cardNo)
        card.assignLimit(200)
        creditCardRepository.save(card)
    }

    def 'new withdrawal should be visible'() {
        given:
            withdrawalResource.withdraw(new WithdrawalRepresentation(100, cardNo))
        when:
            List<WithdrawalRepresentation> result =
                    withdrawalResource.list(cardNo)
        then:
            result.size() == 1

    }
}
